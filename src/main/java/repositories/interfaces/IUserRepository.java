package repositories.interfaces;

import domain.models.User;

public interface IUserRepository extends IEntityRepository<User> {
    User getUserByID(long id);

    User getUserByUsername(String username);
}

package controllers;

import domain.models.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import services.UserService;
import services.interfaces.IUserService;

public class BotController extends TelegramLongPollingBot {
    private final IUserService userService = new UserService();
    private final String TOKEN = "1124784047:AAGqsVFirueR9o88uXiSDxj7HZCfTOlbHNk";
    private final String USERNAME = "ChelichekBot";
    private String username;
    private String password;
    private boolean currentUser=false;



    @Override
    public void onUpdateReceived(Update update) {
        Message receivedMessage = update.getMessage();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(receivedMessage.getChatId());
        String name = receivedMessage.getFrom().getFirstName();
        String surname = receivedMessage.getFrom().getLastName();
        surname = (surname != null ? surname : "");

        if(currentUser==false) {
            if (receivedMessage.getText().equals("/start")) {
                sendMessage.setText("Sign in:" + "\n" + "username:<<your username>>");
            }
            if (receivedMessage.getText().startsWith("username:")) {
                String[] takenmsg = receivedMessage.getText().split(":");
                username = takenmsg[1];
                sendMessage.setText("Write your password:" + "\n" + "password:<<your password>>");
            }
            if(username!=null) {
                if (receivedMessage.getText().startsWith("password:")) {
                    String[] takenmsg = receivedMessage.getText().split(":");
                    password = takenmsg[1];
                    if (setUser(username, password)) {
                        sendMessage.setText("Congrats!");
                    } else {
                        sendMessage.setText("NotCongrats!");
                    }

                }
            }
        }
//        else {
//            String command = update.getMessage().getText();
//            SendMessage message = new SendMessage();
//            if(command.equals("/mygroup")){
//                message.setText(//надо вызвать функцию которая вернет группу//);
//            }
//            if(command.equals("/mydisciplines")){
//                message.setText(//надо вызвать функцию которая вернет список дисциплин//);
//            }
//            if(command.equals("/diciplines_teacher")){
//                message.setText(//надо вызвать функцию которая вернет учителя//);
//            }
//            if(command.equals("/watch_homeworks")){
//                message.setText(//надо вызвать функцию которая вернет список домашок//);
//            }
//            if(command.equals("/my_schedule")){
//                message.setText(//надо вызвать функцию которая вернет рассписание//);
//            }
//            message.setChatId(update.getMessage().getChatId());
//            try {
//                execute(message);
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//        }
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean setUser(String username,String password){
//        if(login(username,password)){
//            currentUser=true;
//              return true;
//        }else{
            currentUser=false;
            return false;
//        }
    }
//        part 2
//        if(update.hasMessage()) {
//            Message receivedMessage = update.getMessage();
//            SendMessage sendMessage = new SendMessage();
//            sendMessage.setChatId(receivedMessage.getChatId());
//
//            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
//            List<List<InlineKeyboardButton>> table = new ArrayList<>();
//
//            List<InlineKeyboardButton> buttonRaw = new ArrayList<>();
//            InlineKeyboardButton btn = new InlineKeyboardButton();
//
//            btn.setText("Hello").setCallbackData("Hello, dear " +
//                    receivedMessage.getFrom().getFirstName());
//            buttonRaw.add(btn);
//
//            Instant instant = Instant.now();
//            btn = new InlineKeyboardButton();
//            btn.setText("Time").setCallbackData(instant.toString());
//            buttonRaw.add(btn);
//
//            table.add(buttonRaw);
//
//            User user = userService.getUserByID(1);
//            buttonRaw = new ArrayList<>();
//            btn = new InlineKeyboardButton();
//            btn.setText("Author").setCallbackData(user.toString());
//            buttonRaw.add(btn);
//
//            btn = new InlineKeyboardButton();
//            btn.setText("Goodbye").setCallbackData("Oh, no, please, don`t go!");
//            buttonRaw.add(btn);
//
//            table.add(buttonRaw);
//
//
//            inlineKeyboardMarkup.setKeyboard(table);
//            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
//            sendMessage.setText("This is test bot...\n\n");
//            try {
//                execute(sendMessage);
//            } catch (TelegramApiException e) {
//                System.out.println(e.getMessage());
//            }
//        } else if (update.hasCallbackQuery()) {
//            try {
//                execute(new SendMessage().setText(
//                        update.getCallbackQuery().getData())
//                        .setChatId(update.getCallbackQuery().getMessage().getChatId()));
//            } catch (TelegramApiException e) {
//                System.out.println(e.getMessage());
//            }
//        }
//    }

    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }
}
